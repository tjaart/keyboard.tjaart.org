module.exports = function(eleventyConfig) {
  // Output directory: public

  eleventyConfig.addPassthroughCopy("img");
};
