# My Keyboard history


## Ergodox (2013)

[My keyboard layout](https://github.com/tjaartvdwalt/qmk_firmware/blob/master/keyboards/ergodox_ez/keymaps/tjaartvdwalt/keymap.c)

My first attempt at building a mechanical keyboard. A friend helped me to make the case from oak. I tried several different firmwares for the Ergodox, but eventually settled on Qmk firmware for all my keyboards.

<img height="400px" src="img/ergodox_1.jpg" />
<img height="400px" src="img/ergodox_2.jpg" />
<img height="400px" src="img/ergodox_3.jpg" />
<img height="400px" src="img/ergodox_4.jpg" />
<img height="400px" src="img/ergodox_5.jpg" />
<img height="400px" src="img/ergodox_6.jpg" />

## Planck (2018)
[My keyboard layout](https://github.com/tjaartvdwalt/qmk_firmware/blob/master/keyboards/ergodox_ez/keymaps/tjaartvdwalt/keymap.c)

I bought the parts from <https://olkb.com>

After a few months of use, I realized that a 40% was just a little to minimalist for me. Having to use modifiers to type keys in the number row proved too much mental overhead, and I switched back to the Ergodox as my primary keyboard. In retrospect I should have waited for the [Preonic](https://olkb.com/collections/preonic) to be released.

In 2023, I went on a 2 month trip to South Africa, and I decide to take my Planck (it is very compact, if not very light weight). After making a few tweaks to the keyboard layout, and getting used to the small size, I had a very productive time with it... maybe all it took was some perseverance.

<img height="400px" src="img/planck_1.jpg" />

## Redox Wireless (2021)
[My keyboard layout](https://github.com/tjaartvdwalt/qmk_firmware/blob/master/keyboards/ergodox_ez/keymaps/tjaartvdwalt/keymap.c)

Even though Redox is a split keyboard design, like the Ergodox, I bought a [joined case from Falbatech](https://falba.tech/product/redox-wireless-joined-lift-bamboo-wood-case-ver-1/). I really love this case, the two sides are placed at exactly the right angle for me, and because you don't have two loose parts, it makes the keyboard much more elegant, in my opinion.

The Redox wireless solved most of the issues I had with the Ergodox. 
- It does not have the annoying wires.
- The thumb cluster has fewer keys, (but still enough), making the keyboard feel more compact than the Ergodox

I did learn a hard lesson on this build: Bluetooth controllers are not easy to solder.


<img height="400px" src="img/redox_1.jpg" />
<img height="400px" src="img/redox_2.jpg" />
<img height="400px" src="img/redox_3.jpg" />
<img height="400px" src="img/redox_4.jpg" />
<img height="400px" src="img/redox_5.jpg" />
